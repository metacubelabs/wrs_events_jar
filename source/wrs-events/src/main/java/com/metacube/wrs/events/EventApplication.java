package com.metacube.wrs.events;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EventApplication {

	private String appName;
	private List<String> topics;
	
}
