package com.metacube.wrs.events;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.json.JSONObject;

import lombok.Getter;

@Getter
class EventListener implements MessageListener {
 
	private EventProcessor eventProcessor;
	
	public EventListener(EventProcessor eventProcessor) {
		this.eventProcessor = eventProcessor;
	}
	
    @Override
    public void onMessage(Message message) {
        try {
            JSONObject jsonObject = new JSONObject(((TextMessage)message).getText());
            JSONObject data =  new JSONObject(jsonObject.getString("Message"));
            if (eventProcessor != null) {
            	eventProcessor.process(data);
            } else {
            	System.out.println(String.format("UnHandled message => '%s'", data.toString()));
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}