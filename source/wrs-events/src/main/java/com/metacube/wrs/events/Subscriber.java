package com.metacube.wrs.events;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Subscriber {
	private AmazonSQS client;
	private Map<String, String> topics;
	private QueueProps queue;
	private Events events;
	private SQSConnection connection;
	private EventProcessor eventProcessor;

	public Subscriber(Events events) {
		this.events = events;
	}

	public void initiate() {
		if (client == null) {
			this.client = events.getSubscriber();
		}
		if (!events.getConfiguration().isDisable()) {
			createApplicationQueue();
			initializeTopicsForSubscription();
		}
	}

	public void startListener() throws JMSException {
		if (connection != null) {
			return;
		}
		Configuration config = events.getConfiguration();
		AWSCredentials credentials = new BasicAWSCredentials(config.getAwsAccessKeyId(),
				config.getAwsSecretAccessKey());
		AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
		AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(config.getAwsRegion())
				.withCredentials(credentialsProvider).build();
		SQSConnectionFactory connectionFactory = new SQSConnectionFactory(new ProviderConfiguration(), sqs);
		connection = connectionFactory.createConnection();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//  		AmazonSQSMessagingClientWrapper client = connection.getWrappedAmazonSQSClient();
		Queue queue = session.createQueue(events.getSubscriberService().applicationQueueName());
		MessageConsumer consumer = session.createConsumer(queue);
		// Instantiate and set the message listener for the consumer.
		consumer.setMessageListener(new EventListener(eventProcessor));

		// Start receiving incoming messages.
		connection.start();
	}

	public void clearListeners() throws JMSException {
		if (connection != null) {
			connection.close();
			connection = null;
		}
	}

	public void subscribeToTopic(String topicArn, String topic) {

		boolean newTopic = topics.containsKey(topic);
		if (newTopic) {
			this.topics.put(topic, topicArn);
		}
		events.getPublisher().subscribe(topicArn, Events.SUBSCRIBER_PROTOCOL, applicationQueueArn());

//      # DEBUG: Topic-wise permissions are disabled as with dynamic event subscription support, a list of subsciptions needs to be maintained in order to support it.
	}

	private void permitTopics(Collection<String> topicArns) {
		Map<String, String> attr = new HashMap<>();
		attr.put("Policy", policyTemplateAllowTopics(applicationQueueArn(), topicArns));
		this.client.setQueueAttributes(applicationQueueUrl(), attr);
	}

	private List<String> subscriptionList() {
		return events.getConfiguration().getSubscribeTo().stream().map(s -> s.getTopics().stream().map((t) -> {
			Matcher matcher = matchingPattern(t);
			if (matcher.find()) {
				List<String> allArns = events.getPublisherService().getAllTopics().stream()
						.filter(tArn -> tArn.contains(
								events.getPublisherService().absoluteTopicName(matcher.group(1), s.getAppName())))
						.map(tArn -> topicNameFromArn(tArn)).collect(Collectors.toList());
				return allArns;
			} else {
				return Arrays.asList(events.getPublisherService().absoluteTopicName(t, s.getAppName()));
			}
		}).flatMap(List::stream).collect(Collectors.toList())).flatMap(List::stream).collect(Collectors.toList());
	}

	private Matcher matchingPattern(String topicName) {
		Pattern pattern = Pattern.compile("/(.*?)(\\-\\*\\-?)(.*)/");
		Matcher matcher = pattern.matcher(topicName);
		return matcher;
	}

	private String topicNameFromArn(String tArn) {
		String[] arr = tArn.split(":");
		return arr[arr.length - 1];
	}

	public String applicationQueueName() {
		return String.join("_", events.getConfiguration().getAppName(), events.getConfiguration().getQueueSuffix());
	}

	public String applicationQueueArn() {
		return queue.getAttributes().getAttributes().get("QueueArn");
	}

	private String applicationQueueUrl() {
		return queue.getProperties().getQueueUrl();
	}

	// TODO: Create WRS Queue Wrapper
	public QueueProps getQueueDetails(String queueName) {
		GetQueueUrlResult properties = client.getQueueUrl(queueName);
		GetQueueAttributesResult attributes = client.getQueueAttributes(properties.getQueueUrl(), Arrays.asList("All"));
		return new QueueProps(properties, attributes);
	}

	// TODO: Create Wrapper classes to hold templates
	public String policyTemplateAllowTopics(String queueArn, Collection<String> topicArns) {
		// DEBUG: Topic-wise permissions are disabled as with dynamic event subscription
		// support, a list of subscriptions needs to be maintained in order to support
		// it.
		return "{\n" + "  \"Version\": \"2012-10-17\",\n" + "  \"Statement\": [\n" + "    {\n"
				+ "      \"Effect\": \"Allow\",\n" + "      \"Principal\": \"*\",\n"
				+ "      \"Action\": \"SQS:SendMessage\",\n" + "      \"Resource\": \"" + queueArn + "\",\n"
				+ "      \"Condition\": {\n" + "        \"ArnLike\": {\n"
				+ "          \"aws:SourceArn\": \"arn:aws:sns:" + events.getConfiguration().getAwsRegion() + ":*:*\"\n"
				+ "        }\n" + "      }\n" + "    }\n" + "  ]\n" + "}\n" + "";
	}

	private void createApplicationQueue() {
		CreateQueueResult res = this.client.createQueue(applicationQueueName());
		if (res == null || BasicUtility.blank(res.getQueueUrl())) {
			throw new IllegalArgumentException("Application queue can't be initialized");
		}

		setApplicationQueue();
		permitTopics(new LinkedList<>());
	}

	public void setApplicationQueue() {
		this.queue = getQueueDetails(applicationQueueName());
	}

	private void initializeTopicsForSubscription() {
		if (topics == null) {
			topics = new HashMap<>();
		}
		List<String> topicsToSubscribe = subscriptionList();

		for (String topicName : topicsToSubscribe) {
			CreateTopicResult res = events.getPublisherService().getClient().createTopic(topicName);
			if (res == null) {
				throw new IllegalStateException();
			}
			topics.put(topicName, res.getTopicArn());
		}

		createQueueSubscriptions();
	}

	private void createQueueSubscriptions() {
		for (Entry<String, String> topic : topics.entrySet()) {
			subscribeToTopic(topic.getValue(), topic.getKey());
		}
		permitTopics(topics.values());
		// DEBUG: Topic-wise permissions are disabled as with dynamic event subscription
		// support, a list of subsciptions needs to be maintained in order to support
		// it.
	}
}

@Getter
@Setter
@AllArgsConstructor
class QueueProps {
	private GetQueueUrlResult properties;
	private GetQueueAttributesResult attributes;
}