package com.metacube.wrs.events;

import java.util.LinkedList;
import java.util.List;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Configuration {
	private String awsAccessKeyId;
	private String awsSecretAccessKey;
	private String awsRegion;
	private String appName;
	private String queueSuffix;

	private List<EventApplication> publishTo;
	private List<EventApplication> subscribeTo;
	private boolean disable = false;
	private Events events;
	
	public Configuration(Events events) {
		this.events = events;
	}

	public void initialize() {
		awsAccessKeyId = null;
		awsSecretAccessKey = null;
		awsRegion = null;
		appName = null;
		queueSuffix = null; // Env name ('staging' | 'development' | 'production') 
//      @event_handler = 'EventHandler'
		disable = false;
		publishTo = new LinkedList<>();
		subscribeTo = new LinkedList<>();
	}

	public void initializePlatform() {
		configurePlatform();
		initializePlatformClients();
	}

	public void configurePlatform() {
		awsAccessKeyId = System.getenv("AWS_ACCESS_KEY");
		awsSecretAccessKey = System.getenv("AWS_SECRET_ACCESS_KEY");
		awsRegion = System.getenv("AWS_REGION");
	}

	public void initializePlatformClients() {
		AWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretAccessKey);
		AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
		events.setSubscriber(AmazonSQSClientBuilder.standard().withRegion(awsRegion).withCredentials(credentialsProvider).build());
		events.setPublisher(AmazonSNSClientBuilder.standard().withRegion(awsRegion).withCredentials(credentialsProvider).build());
	}

	public void valid() {
		awsCredentialsValid();
	}

	public void awsCredentialsValid() {
		if (BasicUtility.blank(awsAccessKeyId) || BasicUtility.blank(awsSecretAccessKey) || BasicUtility.blank(awsRegion)) {
			throw new IllegalArgumentException();
		}
	}
}
