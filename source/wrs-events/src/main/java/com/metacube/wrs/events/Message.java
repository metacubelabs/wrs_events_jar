package com.metacube.wrs.events;

import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message extends Base {

	private JSONObject subject;
	private JSONObject action;
	private JSONObject data;
	private JSONObject meta;

	public static Message from(String payLoad) {
		JSONObject json = new JSONObject(payLoad).getJSONObject("message");
		return new Message(json.getJSONObject("subject"), json.getJSONObject("action"), json.getJSONObject("data"), json.getJSONObject("meta"));
	}
	
	public Message(JSONObject subject, JSONObject action, JSONObject data) {
		this(subject, action, data, new JSONObject());
	}
	
	public Message(JSONObject subject, JSONObject action, JSONObject data, JSONObject meta) {
		super();
		this.subject = subject;
		this.action = action;
		this.data = data;
		this.meta = meta;
	}
	
	public boolean isValid() {
		super.isValid();
		return isSubjectValid() && isActionValid() && isDataValid();
	}

	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		return json.put("subject", subject)
			.put("action", action)
			.put("data", data)
			.put("meta", meta);
	}


	public boolean isSubjectValid() {
		if (!(subject != null && subject.has("id") && subject.has("type"))) {
			addError("subject");
			return false;
		}
		return true;
	}

	public boolean isActionValid() {
		if (!(action != null && action.has("app_name") && action.has("type"))) {
			addError("action");
			return false;
		}
		return true;
	}

	public boolean isDataValid() {
		new JSONObject(action);
		return true;
	}
}
