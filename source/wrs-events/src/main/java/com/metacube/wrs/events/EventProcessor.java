package com.metacube.wrs.events;

import org.json.JSONObject;

public interface EventProcessor {
	
	public void process(JSONObject data);

}
