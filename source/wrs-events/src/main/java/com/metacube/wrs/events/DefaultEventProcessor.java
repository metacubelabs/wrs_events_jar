package com.metacube.wrs.events;

import org.json.JSONObject;

public class DefaultEventProcessor implements EventProcessor {

	@Override
	public void process(JSONObject data) {
		System.out.println(String.format("Handled message => '%s'", data.toString()));
	}

}
