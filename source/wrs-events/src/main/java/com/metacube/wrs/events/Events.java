package com.metacube.wrs.events;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sqs.AmazonSQS;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Events extends Base {
	private AmazonSQS subscriber;
	private AmazonSNS publisher;
	private Configuration configuration;
	private Subscriber subscriberService;
	private Publisher publisherService;
	
	private EventProcessor eventProcessor;

	public static final String SUBSCRIBER_PROTOCOL = "sqs";
	public static final String PUBLISHER_PROTOCOL = "sns";
	private String applicationQueueUrl;
	
	public Events() {
		this.configuration = new Configuration(this);
		this.publisherService = new Publisher(this);
		this.subscriberService = new Subscriber(this);
	}
	
	public Events(EventProcessor eventProcessor) {
		this();
		this.subscriberService.setEventProcessor(eventProcessor);
	}
	
	public void initialize() {
		this.configuration.initializePlatform();
	}

	public void reset() {
		configuration = new Configuration(this);
		publisher = null;
		subscriber = null;
	}
}
