package com.metacube.wrs.events;

public class BasicUtility {

	public static boolean blank(String s) {
		return s == null || s.trim().isEmpty(); 
	}
}
