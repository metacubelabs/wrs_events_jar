package com.metacube.wrs.events;

import java.util.LinkedList;
import java.util.List;

public abstract class Base {
	
	private List<String> errors;
	
	public boolean isValid() {
		this.errors = new LinkedList<>();
		return true;
	}
	
	public boolean isInvalid() {
		return !isValid();
	}
	
	public void addError(String key) {
		errors.add(key);
	}
}
