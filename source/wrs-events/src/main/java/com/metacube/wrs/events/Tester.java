package com.metacube.wrs.events;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;

public class Tester {
	public static void main(String[] args) {
		Events events = new Events(new DefaultEventProcessor());
		List<EventApplication> subscribeTo = new LinkedList<>();
		subscribeTo.add(new EventApplication("wrs_core", Arrays.asList("employee-update")));
		subscribeTo.add(new EventApplication("lnd_test", Arrays.asList("testevent")));
		Configuration config = events.getConfiguration();
		config.setSubscribeTo(subscribeTo);
		config.setAppName("lnd_test");
		config.setQueueSuffix("staging");
//			    # config.queue_suffix = 'development'
//			    config.event_handler = 'EventHandler' # >process_messages'
//			    # config.disable = true
		List<EventApplication> publishTo = new LinkedList<>();
		publishTo.add(new EventApplication("lnd_test", Arrays.asList("testevent")));
		config.setPublishTo(publishTo);
		events.initialize();
		
		EventsService service = new EventsService(events);
		try {
			service.initiate();
			service.publish("testevent", new JSONObject("{\"id\": 1, \"class\": \"random\"}"), new JSONObject("{\"name\":\"Mah\"}"));
			service.publish("testevent", new JSONObject("{\"id\": 1, \"class\": \"random\"}"), new JSONObject("{\"name\":\"Nam\"}"));
			service.startListening();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
