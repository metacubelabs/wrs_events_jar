package com.metacube.wrs.events;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.ListTopicsResult;
import com.amazonaws.services.sns.model.Topic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Publisher {
	
	private AmazonSNS client;
	private Map<String, String> topics;
	private String errors;
	
	private Events events;
	
	public Publisher(Events events) {
		this.events = events;
	}

	public void initiate() {
		if (this.client == null) {
			this.client = events.getPublisher();
		}
		if (!events.getConfiguration().isDisable()) {
			initializeTopicsForPublication();
		}
	}

	public void publish(Message message, String topic) {
		if (events.getConfiguration().isDisable() || !message.isValid()) {
			return;
		}
		
		if (!canPublishTo(topic)) {
			throw new IllegalArgumentException(String.format("Permission denied to publish to %s", topic));
		}
	
		String publishingTopic = absoluteTopicName(topic);
		String publicToARN = topicUid(publishingTopic);

		if (BasicUtility.blank(publicToARN)) {
			throw new IllegalStateException(String.format("Can't fetch topic with name '%s'", topic));
		}
		client.publish(publicToARN, message.toJson().toString());
	}

	public CreateTopicResult findOrCreateTopic(String topicName, String appName) {
      String formattedTopicName = BasicUtility.blank(appName) ? topicName : absoluteTopicName(topicName, appName) ;
      return client.createTopic(formattedTopicName);
	}

	public void deleteTopic(String topicName, String appName) {
	    String formattedTopicName = BasicUtility.blank(appName) ? topicName : absoluteTopicName(topicName, appName);
	    if (!topics.containsKey(formattedTopicName)) {
	    	return;
	    }
	    client.deleteTopic(topics.get(formattedTopicName));
	}

	public List<String> getAllTopics() {
		ListTopicsResult res = client.listTopics();
		List<Topic> allTopics = res.getTopics();
		return allTopics.stream().map(t -> t.getTopicArn()).collect(Collectors.toList());
	}

	public List<String> publicationList() {
		return events.getConfiguration().getPublishTo()
				.stream().map(p -> p.getTopics().stream().map(t -> absoluteTopicName(t, p.getAppName())).collect(Collectors.toList()))
				.flatMap(List::stream).collect(Collectors.toList());
	}

	public String topicUid (String name) {
		Map<String, String> topics = events.getSubscriberService().getTopics();
		return topics.containsKey(name) ? topics.get(name) : (this.topics.containsKey(name) ? this.topics.get(name) : null);
	}
	
	public String absoluteTopicName(String topic) {
		return absoluteTopicName(topic, events.getConfiguration().getAppName());
	}

	public String absoluteTopicName(String topic, String appName) {
		if (BasicUtility.blank(topic)) {
			return null;
		}
		return String.join("_", Arrays.asList(appName, events.getConfiguration().getQueueSuffix(), topic));
	}

    public boolean canPublishTo(String topic) {
    	return publicationList().contains(absoluteTopicName(topic, events.getConfiguration().getAppName()));
    }

	public void initializeTopicsForPublication() {
		if (topics == null) {
			topics = new HashMap<>();
		}
		List<String> topicsToPublish = publicationList();
		for (String topicName: topicsToPublish) {
			CreateTopicResult res = findOrCreateTopic(topicName, null);
			if (res == null) {
				throw new IllegalStateException();
			}
			topics.put(topicName, res.getTopicArn());
		}
	}

}
