	package com.metacube.wrs.events;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.jms.JMSException;

import org.json.JSONObject;

import com.amazonaws.services.sns.model.CreateTopicResult;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EventsService {
	private Events events;

	public void initiate() {
		events.getPublisherService().initiate();
		events.getSubscriberService().initiate();
//    Wrs::Events::Subscriber.initiate
	}

	public void startListening() throws JMSException {

		this.events.getSubscriberService().startListener();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				System.out.println("Terminating Event Listener");
				this.events.getSubscriberService().clearListeners();
				System.out.println("Event Listener Terminated");
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}));
	}

	public void createTopicSubscription(String eventType, String appName) {
		if (BasicUtility.blank(eventType) || BasicUtility.blank(appName)) {
			throw new IllegalArgumentException("One of topic or app name is missing");
		}
		CreateTopicResult res = events.getPublisherService().findOrCreateTopic(eventType, appName);
		if (res == null) {
			throw new IllegalStateException("Error while creating topic");
		}
		events.getSubscriberService().subscribeToTopic(res.getTopicArn(), eventType);
	}

	public void deleteTopicSubscription(String eventType, String appName) {
		if (BasicUtility.blank(eventType) || BasicUtility.blank(appName)) {
			throw new IllegalArgumentException("One of topic or app name is missing");
		}
		events.getPublisherService().deleteTopic(eventType, appName);
	}

	public void updateTopicSubscription(String eventType, String appName) {
		deleteTopicSubscription(eventType, appName);
		createTopicSubscription(eventType, appName);
	}

	public void publish(String eventType, JSONObject subject, JSONObject data) {
		JSONObject action_h = new JSONObject();
		action_h.put("type", eventType);
		action_h.put("app_name", events.getConfiguration().getAppName());
		JSONObject meta_h = new JSONObject();
		// Sample timestamp: 2020-06-15T13:37:01.353+05:30
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		meta_h.put("timestamp", ZonedDateTime.now().format(dtf));
		JSONObject subject_h = new JSONObject();
		subject_h.put("id", subject.get("id"));
		subject_h.put("type", subject.get("class"));
		Message message = new Message(subject_h, action_h, data, meta_h);
		events.getPublisherService().publish(message, eventType);
	}
}
